package com.sda.strukturyDanych.zadanie01;
//Dodaj 5 imion do kolekcji. Wybierz tą kolekcję, która posortuje wprowadzone
//        wartość. Po dodaniu elementów wyświetl wszystkie.

import java.util.TreeSet;

public class Main {
    public static void main(String[] args) {
        TreeSet<String> listaImion = new TreeSet<String>();
        listaImion.add("jasiu");
        listaImion.add("marysia");
        listaImion.add("krysia");
        listaImion.add("adam");
        listaImion.add("zdzisław");
        System.out.println(listaImion);
        for (String imie:listaImion) {
            System.out.println(imie);

        }
    }
}
