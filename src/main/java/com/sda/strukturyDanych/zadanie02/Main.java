package com.sda.strukturyDanych.zadanie02;

import java.util.ArrayList;
import java.util.Random;

//Dodaj 10 losowych liczb do kolekcji. Korzystając z indeksów pobierz pokolei
//        wszystkie elementy i wyświetl je.
public class Main {
    public static void main(String[] args) {
        ArrayList<Integer> liczbyLosowe =new ArrayList<Integer>();
        //losujemy
        Random randomGenerator= new Random();
        for (int i=0;i<10;i++){
              liczbyLosowe.add(randomGenerator.nextInt(100)); //losuje od 0 do 99 włącznie
        }
        //wyswietlamy
        for (int i = 0; i < liczbyLosowe.size(); i++) {
            System.out.println(liczbyLosowe.get(i));
        }
    }
}
