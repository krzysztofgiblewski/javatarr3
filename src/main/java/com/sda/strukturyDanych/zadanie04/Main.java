package com.sda.strukturyDanych.zadanie04;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

/*Jesteś firmą produkującą tabliczki z imionami. Dla każdego imienia musisz
przygotować matrycę. Raz przygotowana matryca może być wykorzystywana
wielokrotnie. Korzystając z odpowiedniej kolekcji dodaj do niej imiona (co najmniej
10) osób tak aby w kolekcji się nie powtarzały. Podczas dodawania dodaj kilka imion
powtarzających się.*/
public class Main {
    public static void main(String[] args) {

        List<String> listaImion=new ArrayList<>();
        listaImion.add("Jan");
        listaImion.add("Jan");
        listaImion.add("Adrian");
        listaImion.add("Zenonn");
        listaImion.add("Jan");
        listaImion.add("Barbara");
        listaImion.add("Barbara");
        listaImion.add("Zofia");
        listaImion.add("Ola");
        listaImion.add("Wojtek");
        listaImion.add("Maciej");
        listaImion.add("Kasia");
        listaImion.add("Kasia");
        listaImion.add("Karol");


        TreeSet<String> nowaLista = new TreeSet<>();

        nowaLista.add("Jan");
        nowaLista.add("Jan");
        nowaLista.add("Adrian");
        nowaLista.add("Zenonn");
        nowaLista.add("Jan");
        nowaLista.add("Barbara");
        nowaLista.add("Barbara");
        nowaLista.add("Zofia");
        nowaLista.add("Ola");
        nowaLista.add("Wojtek");
        nowaLista.add("Maciej");
        nowaLista.add("Kasia");
        nowaLista.add("Kasia");
        nowaLista.add("Karol");

        System.out.print(listaImion.size());
        System.out.println(" elementów listy ArrayList: ");
        for (String i:listaImion) { //petla for i
            System.out.println(i);
        }
        System.out.print(" bez powtórzeń "+nowaLista.size()+" elementów mimoże lista imion zawiera takie same imiona ");
        System.out.println(" to bedzie odpowiednia kolekcja TreeSet: ");
        for (String i:nowaLista) { //petla for i
            System.out.println(i);
        }

    }


}
