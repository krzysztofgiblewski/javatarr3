package com.sda.strukturyDanych.zadanie03;
import java.util.LinkedList;
import java.util.Queue;

/*
Utwórz kolejkę osób do lekarza. Dodaj 10 losowych osób i wyświetl pierwsza i ostatnią osobę z kolejki.
Utwórz klasę Person dla osoby posiadającą właściwości name oraz lastName.
Nie korzystaj z listy ArrayList
 */
public class Main {
    public static void main(String[] args) {
        Queue<Person> kolejkaOsob = new LinkedList<>();
        kolejkaOsob.add(new Person("Łukasz", "Sierakowski"));
        kolejkaOsob.add(new Person("Jan", "Dąbrowski"));
        kolejkaOsob.add(new Person("Michał", "Kowalski"));
        kolejkaOsob.add(new Person("Wojciech", "Kolanko"));
        kolejkaOsob.add(new Person("Kamil", "Brzęczeszczykiewicz"));
        kolejkaOsob.add(new Person("Paweł", "Lato"));
        kolejkaOsob.add(new Person("Stanisław", "Głębocki"));
        kolejkaOsob.add(new Person("Kamila", "Rodzik"));
        kolejkaOsob.add(new Person("Monika", "Rymanowska"));
        kolejkaOsob.add(new Person("Ewa", "Jerenko"));
        Person pierwszaOsoba = kolejkaOsob.peek();
        Person ostatniaOsoba = null;
        int iloscOsobWKolejsce = kolejkaOsob.size();
        for (int i = 0; i < iloscOsobWKolejsce; i++) {
            ostatniaOsoba = kolejkaOsob.poll();
        }
        System.out.println("Pierwsza osoba: " + pierwszaOsoba.getName()
                + " " + pierwszaOsoba.getLastName());
        if (ostatniaOsoba != null) {
            System.out.println("Ostatnia osoba: " + ostatniaOsoba.getName()
                    + " " + ostatniaOsoba.getLastName());
        }
    }
}
