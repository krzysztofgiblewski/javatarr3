package com.sda.dataICzas.zadanie06;

import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.ZoneId;

/**
 * Wyświetl bieżącą datę i godzinę w Tokyo
 */
public class Main {
    public static void main(String[] args) {
        ZoneId strefaCzasowaTokyo=ZoneId.of("Asia/Tokyo"); //ustawiam strefę czasową na tokyo
        LocalDateTime tokijskiCzas=LocalDateTime.now(strefaCzasowaTokyo);
        System.out.println(tokijskiCzas);
    }
}
