package com.sda.dataICzas.zadanie05;

import java.sql.Time;
import java.time.Duration;
import java.time.LocalTime;

/**
 * Utwórz obiekt typu LocalTime przechowujący godzinę 14:11 oraz obiekt typu
 * LocalTime przechowujący godzinę 18:41. Wykorzystując klasę Duration oblicz ile
 * czasu upłynęło pomiędzy godzinami.
 */
public class Main {
    public static void main(String[] args) {
        LocalTime godzinaCzternasta= LocalTime.of(14,11) ;
        LocalTime godzinaOsiemnasta=LocalTime.of(18,41);

        System.out.println(Duration.between(godzinaOsiemnasta,godzinaCzternasta));
    }
}
