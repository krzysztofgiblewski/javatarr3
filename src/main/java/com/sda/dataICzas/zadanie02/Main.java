package com.sda.dataICzas.zadanie02;

import java.time.LocalDate;

/**
 * Wykorzystując obiekt typu LocalDate wyświetl aktualną datę.
 */
public class Main {
    public static void main(String[] args) {
        LocalDate date=LocalDate.now();
        System.out.println(date.getYear()+"-"+date.getMonth()+"-"+date.getDayOfMonth());
    }
}
