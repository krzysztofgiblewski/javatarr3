package com.sda.dataICzas.zadanie03;

import java.time.LocalDateTime;

/**
 * Wykorzystując obiekt typu ​LocalDateTime​ wyświetl aktualną datę i godzinę
 */
public class Main {
    public static void main(String[] args) {
        LocalDateTime dataCzas=LocalDateTime.now();
        System.out.println(dataCzas);
    }
}
