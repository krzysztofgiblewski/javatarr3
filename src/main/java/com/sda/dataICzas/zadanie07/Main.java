package com.sda.dataICzas.zadanie07;

import java.time.LocalDateTime;

/**
 * Wyświetla bieżącą godzinę w Bydgoszczy. Wykorzystaj DateTimeFormatter aby
 * wyświetlić datę w następującym formacie
 * 3 lutego 2018 roku, sobota 22:12:27
 */
public class Main {
    public static void main(String[] args) {
        LocalDateTime dataCzasTeraz=LocalDateTime.now();
//        (dataCzasTeraz.getDayOfMonth()+" "+dataCzasTeraz.getDayOfMonth()+" "+dataCzasTeraz.getYear()+" roku, "+ dataCzasTeraz.getDayOfWeek()+ dataCzasTeraz.getHour()+":"+dataCzasTeraz.getMinute()+":"+dataCzasTeraz.getMinute());

        System.out.println(dataCzasTeraz);
    }
}
