package com.sda.dataICzas.zadanie04;

import java.time.LocalDate;
import java.time.Period;

/**
 * Utwórz obiekt typu LocalDate przechowujący datę 01.01.2017 oraz obiekt typu
 * LocalDate przechowujący datę 05.05.2017. Wykorzystując obiekt typu Period
 * Wyświetl ile czasu minęło pomiędzy datami.
 */
public class Main {
    public static void main(String[] args) {
        LocalDate pierwszaData , drugaData;
        pierwszaData=LocalDate.of(2017,01,01);
        drugaData=LocalDate.of(2017,05,05);
        Period period=Period.between(pierwszaData,drugaData);
        System.out.println(period.getDays()+" Dni rużnicy ");
        System.out.println(period.getMonths()+" Miesięcy rużnicy ");
        System.out.println(period.getYears()+" Lat rużnicy ");

    }
}
