package com.sda.dataICzas.zadanie01;

import java.time.LocalTime;

/**
 * Wykorzystując obiekt typu LocalTime wyświetl aktualny czas.
 */
public class Main {
    public static void main(String[] args) {
        LocalTime aktualnyCzas= LocalTime.now();
        System.out.println(aktualnyCzas.getHour()+":"+aktualnyCzas.getMinute()+":"+aktualnyCzas.getSecond());
    }
}
