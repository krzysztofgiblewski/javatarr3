package com.sda.oop.zadanie03;

public class SportCar extends Car{
    public SportCar(String producer, String model, String color, int seatsNumber) {
        super(producer, model, color, seatsNumber);
    }

    public SportCar() {
    }
}
