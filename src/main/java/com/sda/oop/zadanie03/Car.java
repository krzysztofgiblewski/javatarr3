package com.sda.oop.zadanie03;

public abstract class Car {
    String producer, model, color;
    int seatsNumber;

    public Car(String producer, String model, String color, int seatsNumber) {
        this.producer = producer;
        this.model = model;
        this.color = color;
        this.seatsNumber = seatsNumber;
    }

    public Car() {
        this.seatsNumber = 2;
    }


}
