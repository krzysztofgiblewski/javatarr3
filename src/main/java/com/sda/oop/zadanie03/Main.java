package com.sda.oop.zadanie03;

/**
 * Utwórz klasę Engine
 * a. Dodaj pola capacity, horsePower, fuelConsumption.
 * b. Utwórz konstruktor przyjmujący wszystkie możliwe parametry do ustawienie
 * klasy Engine
 * c. Utwórz metody umożliwiające ustawienie każdego parametru/pola klasy
 * Engine
 * d. Utwórz metody umożliwiające ustawienie każdego parametry/pola klasy
 * Engine
 * e. Utwórz klasę abstrakcyjną Car posiadającą pola producer, model, color,
 * seatsNumber oraz Engine.
 * f. Utwórz klasę SportCar dziedziczącą po klasie Car.
 * g. Utwórz konstruktor bezparametrowy w klasie Car, który inicjalizuje pole
 * seatsNumber wartością 2.
 * h. Utwórz konstruktor przyjmujący wszystkie możliwe parametry do ustawienie
 * klasy Car.
 * i. Utwórz konstruktor przyjmujący wszystkie możliwe parametry do ustawienie
 * klasy SportCar.
 * j. Zmodyfikuj utworzony konstruktor tak aby wywołał wieloparametrowy
 * konstruktor klasy Car
 */
public class Main {
    public static void main(String[] args) {

    }


}
