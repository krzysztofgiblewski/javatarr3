package com.sda.oop.zadanie03;

public class Engine {
    int capacity, horsePower, fueiConsumption;

    public int getCapacity() {
        return capacity;
    }

    public void setCapacity(int capacity) {
        this.capacity = capacity;
    }

    public int getHorsePower() {
        return horsePower;
    }

    public void setHorsePower(int horsePower) {
        this.horsePower = horsePower;
    }

    public int getFueiConsumption() {
        return fueiConsumption;
    }

    public void setFueiConsumption(int fueiConsumption) {
        this.fueiConsumption = fueiConsumption;
    }

    public Engine(int capacity, int horsePower, int fueiConsumption){
     this.capacity=capacity;
     this.horsePower=horsePower;
     this.fueiConsumption=fueiConsumption;
    }
}
