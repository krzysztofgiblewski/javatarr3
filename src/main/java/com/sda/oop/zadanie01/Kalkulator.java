package com.sda.oop.zadanie01;

public class Kalkulator {


    public int sum(int value1, int value2) {
        int wynikDodawania = value1 + value2;
        return wynikDodawania;
    }

    public int ruznica(int value1, int value2) {
        int wynikOdejmowania = value1 - value2;
        return wynikOdejmowania;
    }

    public int mnoze(int value1, int value2) {
        int wynikMnozenia = value1 * value2;
        return wynikMnozenia;
    }
    public double dzielenie(double value1, double value2){
        double wynikDzielenia=value1/value2;
        return wynikDzielenia;
    }

}
