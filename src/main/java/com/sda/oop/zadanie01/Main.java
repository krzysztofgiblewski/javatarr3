package com.sda.oop.zadanie01;

import com.sda.oop.zadanie01.Kalkulator;

import java.util.Scanner;

/**
 * Utwórz klasę Calculator, a w niej metodę sumującą 2 liczby całkowite. Metoda
 * powinna zwracać sumę podanych 2 liczb jaki również liczbę całkowitą. Sygnatura
 * (definicja) metody jest następująca.
 * public int sum(int value1, int value2)
 * Analogicznie dodaj metodę umożliwiającą odejmowanie, dzielenie i mnożenie.
 * Po wykonaniu czynności klasa Calculator będzie posiadała 4 metody publiczne
 * umożliwiające wykonywanie podstawowych działań matematycznych.
 * Utwórz klasę Main umożliwiającą uruchomienie programu, następnie utwórz w niej
 * metodę main, i zaprezentuj użycie klasy kalkulator.
 * Założenie: Program powinien umożliwiać wprowadzenie 2 liczb przez użytkownika
 * oraz zdefiniowanie rodzaju operacji jaka ma zostać wykonana (+, -, / oraz *). Po
 * wykonaniu operacji powinien wyświetlić wynik operacji.
 */
public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int pierwszaLiczba;
        int drugaLiczba;
        System.out.println("wprowadz pierwszą liczbę");
        pierwszaLiczba = scanner.nextInt();
        System.out.println("podaj drugą liczbę");
        drugaLiczba = scanner.nextInt();

        System.out.println("podaj operację jaką chcesz wykonać + - * /");
        String operacja = scanner.next();
        Kalkulator kalkulator = new Kalkulator();
        switch (operacja) {
            case "+":
                System.out.println(pierwszaLiczba + " + " + drugaLiczba + " = " + kalkulator.sum(pierwszaLiczba, drugaLiczba));
                break;
            case "-":
                System.out.println(pierwszaLiczba + " - " + drugaLiczba + " = " + kalkulator.ruznica(pierwszaLiczba, drugaLiczba));
            break;
            case "*":
                System.out.println(pierwszaLiczba + " * " + drugaLiczba + " = " + kalkulator.mnoze(pierwszaLiczba, drugaLiczba));
            break;
            case"/" :
                System.out.println(pierwszaLiczba + " / " + drugaLiczba + " = " + kalkulator.dzielenie(pierwszaLiczba, drugaLiczba));
            break;
        }

    }


}


