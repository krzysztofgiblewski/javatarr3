package com.sda.oop.zadanie04;

public class Circle extends Figure {
  int radius;

  public Circle(){

  }
    public Circle(int radius) {
        this.radius = radius;
    }

    @Override
    public float countArea(){
        return (float) (Math.PI *(radius*radius));

    }

    @Override
    public float displayArea() {
        System.out.println("pole koła o promieniu "+radius+ " wynosi "+countArea());
        return countArea();
    }

    public int getRadius() {
        return radius;
    }

    public void setRadius(int radius) {
        this.radius = radius;
    }
}
