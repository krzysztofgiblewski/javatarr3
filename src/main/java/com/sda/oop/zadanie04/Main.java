package com.sda.oop.zadanie04;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.Scanner;

/**
 * *Zadanie na tworzenie odpowiedniego typu
 * a. Utwórz klasę abstrakcyjną Figure posiadającą metodę abstrakcyjną
 * float countArea()
 * b. Utwórz metodę abstrakcyjną void displayArea() w klasie Figure.
 * c. Utwórz klasę Rectangle dziedziczącą po klasie Figure
 * i. dodaj pole sideA będące długością boku prostokąta.
 * ii. dodaj pole sideB będące długością boku prostokąta.
 * iii. nadpisz metodę countArea() klasy Figure i zaimplementuj ją.
 * iv. Utwórz konstruktor przyjmujący wszystkie parametry potrzebne do
 * obliczenia pola.
 * v. nadpisz metodę displayArea() tak aby wyświetlała informację:
 * “Figura: Prostokąt, pole: <obilczone_pole>”
 * gdzie <obliczone_pole> to wynik wykonania metody countArea()
 * d. Utwórz klasę Square dziedziczącą po klasie Figure
 * i. dodaj pole side będące długością boku kwadratu
 * ii. nadpisz metodę countArea() klasy Figure i zaimplementuj ją.
 * iii. Utwórz konstruktor przyjmujący wszystkie parametry potrzebne do
 * obliczenia pola.
 * iv. nadpisz metodę displayArea() tak aby wyświetlała informację:
 * “Figura: Kwadrat, pole: <obilczone_pole>”
 * gdzie <obliczone_pole> to wynik wykonania metody countArea()
 * e. Utwórz klasę Circle dziedziczącą po klasie Figure
 * i. dodaj pole radius będące promieniem okręgu
 * ii. nadpisz metodę countArea() klasy Figure i zaimplementuj ją.
 * iii. Utwórz konstruktor przyjmujący wszystkie parametry potrzebne do
 * obliczenia pola.
 * iv. nadpisz metodę displayArea() tak aby wyświetlała informację:
 * “Figura: Koło, pole: <obilczone_pole>”
 * gdzie <obliczone_pole> to wynik wykonania metody countArea()
 * f. Pobierz od użytkownika ilość poszczególnych figur (prostokąt, kwadrat, koło)
 * jakie mają zostać wygenerowane.
 * g. Utwórz listę figur o type List<Figure> figures
 * h. Utwórz odpowiednią ilość poszczególnych figur podanych przez użytkownika i
 * dodaj każdą z nich do listy figures. Podczas tworzenia figur wylosuj wartości
 * boków/promienia korzystając z klasy Random
 * i. Wyświetl po kolei powierzchnię każdej figury znajdującej się w tablicy za
 * pomocą pętli for korzystając z metody displayArea()
 */
public class Main {
    public static void main(String[] args) {
//    Rectangle prostokont=new Rectangle();
//    prostokont.setSideA(10);
//    prostokont.setSideB(50);
//    prostokont.displayArea();
//
//    Square kwadrat=new Square();
//    kwadrat.setSide(10);
//    kwadrat.displayArea();
//
//    Circle kolo=new Circle();
//    kolo.setRadius(50);
//    kolo.displayArea();

        Scanner scanner=new Scanner(System.in);
        System.out.println("podaj ile chcesz prostokątów");
        int ileProstokadow =scanner.nextInt();
        System.out.println("podaj ile chcesz kwadratów");
        int ileKwadratow =scanner.nextInt();
        System.out.println("podaj ile chcesz kół");
        int ileKol =scanner.nextInt();
        List<Figure> figurs =new ArrayList<>();
        Random losouj=new Random(); //zainicjować losuj
        for (int i=0;i<ileProstokadow;i++){
            Rectangle prostokat =new Rectangle();
            //urzyć losuj bounds to zakres losowania czylio do 100
            prostokat.setSideA( losouj.nextInt(100));
            prostokat.setSideB( losouj.nextInt(50));
            //dodaje obiekt do Listy<Figure>
            figurs.add(prostokat);
        }
        for (int i=0;i<ileKwadratow;i++){
            Square kwadrat =new Square();
            kwadrat.setSide(losouj.nextInt(100));
            figurs.add(kwadrat);
        }
        for (int i=0; i<ileKol;i++){
            Circle kolo=new Circle();
            kolo.setRadius(losouj.nextInt(100));
            figurs.add(kolo);

        }
        for (int i=0;i<figurs.size();i++){
            figurs.get(i).displayArea();
        }





    }
}
