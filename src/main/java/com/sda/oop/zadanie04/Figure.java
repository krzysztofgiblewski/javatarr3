package com.sda.oop.zadanie04;

public abstract class Figure  {

    public abstract float  countArea(); //metody abstrakcyjne muszą być bez ciała
    public abstract float displayArea();
}
