package com.sda.oop.zadanie04;

public class Rectangle extends Figure {
    private int sideA;
    private int sideB;

    @Override
    public float countArea() {
        return this.sideA * this.sideB;
    }

    @Override
    public float displayArea() {
        System.out.println("Pole prostokonta o boku "+getSideA()+" i boku "+getSideB()+" wynosi " + countArea());
        return countArea();
    }

    public Rectangle() {
        this.sideA = sideA;
        this.sideB = sideB;
    }


    public int getSideA() {
        return sideA;
    }

    public void setSideA(int sideA) {
        this.sideA = sideA;
    }

    public int getSideB() {
        return sideB;
    }

    public void setSideB(int sideB) {
        this.sideB = sideB;
    }
}
