package com.sda.oop.zadanie04;

public class Square extends Figure{
    int side;

    public Square(int side) {
        this.side = side;
    }

    public Square() {

    }

    @Override
    public float countArea() {
        return side*side;
    }

    @Override
    public float displayArea() {
        System.out.println("Pole kwadratu o boku "+side+" wynosi " + countArea());

        return countArea();
    }

    public int getSide() {
        return side;
    }

    public void setSide(int side) {
        this.side = side;
    }
}
