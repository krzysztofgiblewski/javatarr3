package com.sda.oop.zadanie02;

public class Person {

    private Address address;
    private String name, surname, age;

    public Person() {
    }

    public Person(String name, String surname, String age) {
        this.name = name;
        this.surname = surname;
        this.age = age;
    }

    public Person(Address address, String name, String surname, String age) {
        this.address = address;
        this.name = name;
        this.surname = surname;
        this.age = age;
    }

    public Address getAddress() {
        return address;
    }

    public void setAddress(Address address) {
        this.address = address;
    }

    public void introduce() {
        System.out.println("Imię: " + name + "\nNazwisko: " + surname);
    }

    public void introduceAdress() {
        System.out.println(this.name + " " + this.surname + " " + this.age + " " + this.address);
    }


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getAge() {
        return age;
    }

    public void setAge(String age) {
        this.age = age;
    }
}
