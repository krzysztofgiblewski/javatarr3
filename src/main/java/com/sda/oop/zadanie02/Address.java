package com.sda.oop.zadanie02;

import lombok.ToString;

public class Address {
    private String street;
    private String city;
    private String country;
    private String homeNo;


    public Address() {

    }
    public Address(String street, String city, String country, String flatNo, String homeNo) {
        this.street = street;
        this.city = city;
        this.country = country;
        this.flatNo = flatNo;
        this.homeNo = homeNo;
    }
    private String flatNo;


    public void setCity(String city) {
        this.city = city;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public void setFlatNo(String flatNo) {
        this.flatNo = flatNo;
    }

    public void setHomeNo(String homeNo) {
        this.homeNo = homeNo;
    }

    public String getStreet() {
        return street;
    }

    public String getCity() {
        return city;
    }

    public String getCountry() {
        return country;
    }

    public String getFlatNo() {
        return flatNo;
    }

    public String getHomeNo() {
        return homeNo;
    }

    @Override
    public String toString(){
        return "kraj "+country+" miasto "+city+" ulica " +street+" "+flatNo+" "+homeNo;

    }
}
