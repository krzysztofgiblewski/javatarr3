package com.sda.oop.zadanie02;

/**
 * Utwórz klasę Person posiadającą pola name, surname, age.
 * a. Utwórz konstruktor bezparametrowy.
 * b. Utwórz konstruktor przyjmujący wszystkie możliwe parametry do ustawienia
 * klasy Person.
 * c. Dodaj metodę introduce wyświetlającą na konsoli imię oraz nazwisko osoby.
 * d. Utwórz klasę Address. Dodaj do klasy pola street, city, country, flatNo,
 * homeNo.
 * e. Utwórz konstruktor przyjmujący wszystkie możliwe parametry do ustawienia
 * klasy Address (street, city, country, flatNo, homeNo)
 * f. Rozbuduj klasę Person tak aby przechowywała klasę Address.
 * g. Utwórz nowy konstruktor przyjmujący wszystkie możliwe parametry do
 * ustawienia klasy Person (name, surname, age oraz Address)
 * h. Utwórz metody umożliwiające ustawienie każdego parametru/pola klasy
 * Person
 * i. Utwórz metody umożliwiające ustawienie każdego parametry/pola klasy
 * Address
 * j. Utwórz metody umożliwiające pobranie każdego parametru/pola klasy Person
 * k. Utwórz metody umożliwiające pobranie każdego parametru/pola klasy
 * Address
 */
import com.sda.oop.zadanie02.Person;
import com.sda.oop.zadanie02.Address;

public class Main {
    public static void main(String[] args) {

        Address address=new Address() ;
        address.setCity("Toruń");
        address.setCountry("Polska");
        address.setStreet("Szeroka");
        address.setHomeNo("30");
        address.setFlatNo("50");

        Person osoba =new Person();
        osoba.setName("Kasia");
        osoba.setSurname("Kowalska");
        osoba.setAge("40");
        osoba.setAddress(address);

        osoba.introduce();

        osoba.introduceAdress();
    }
}
