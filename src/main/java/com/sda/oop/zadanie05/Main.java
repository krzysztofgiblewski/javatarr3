package com.sda.oop.zadanie05;

/**
 * *** Przygotowywanie pizzy :-)
 * a. Przygotuj interfejs o nazwie Pizza do wypiekania pizzy posiadający jedną
 * metodę void preparePizza(). Zaimplementuj w/w interfejs w klasie Margherita,
 * Hawaiian, Veggie. Niech metoda preparePizza() wyświetla na konsoli kroki
 * jakie należy wykonać aby przygotować pizzę. Każda pizza powinna mieć listę
 * składników, która będzie przechowywana jako lista. Utwórz konstruktor
 * bezparametrowy oraz taki, który umożliwia przekazanie listy składników do
 * każdej pizzy.
 * b. Przygotuj interfejs o nazwie Ingredients do pobierania składników pizzy
 * posiadający metodę List<String> getIngredients() zwracający listę składników
 * i zaimplementuj go w każdej pizzy.
 * c. Przygotuj interfejs do wyrabiania ciasta o nazwie PizzaDough, a w nim
 * metodę void preparePizzaDough().
 * Przygotuj następujące klasy implementujące interfejs PizzaDough
 * i. AllAmericanThin
 * ii. GlutenFree
 * iii. ChessyBites
 * iv. Pan
 * v. FeelGoodFlatbread
 * vi. StuffedCrust
 * Zaimplementuj metodę preparePizzaDough() w każdej klasie. Niech ta
 * metoda wyświetla tekst na konsoli “Przygotowywanie ciasta <nazwa_ciasta>”
 * d. Dodaj zmienną prywatną typu PizzaDough w każdej pizzy i rozszerz
 * konstruktor klasy tak aby przyjmował zmienną PizzaDough jako argument i
 * przypisywał go do utworzonej zmiennej.
 * e. Rozszerz metodę preparePizza() tak aby jako pierwszy krok była
 * wykonywana metoda preparePizzaDough() obiektu przypisanego do zmiennej
 * typu PizzaDough.
 * f. Do wspólnej listy dodaj kilka różnych pizz (po jednej z każdego rodzaju) i do
 * każdej z nich przekaż inne ciasto. Następnie korzystając z pętli for wyświetl
 * sposób ich wykonywania za pomocą metody preparePizza()
 */
public class Main {
    public static void main(String[] args) {
        for(int i=1;i<=6;i++){
            Margherita margherita1 = new Margherita(i);
            margherita1.preparePizza();
            Veggie veggie=new Veggie(i);
            veggie.preparePizza();
            Hawaiian hawaiian=new Hawaiian(i);
            hawaiian.preparePizza();
        }


    }
}
