package com.sda.oop.zadanie05;

import java.util.ArrayList;
import java.util.List;

/**
 * PIZZA MARGARITA
 */

public class Margherita implements Pizza, Ingredients, PizzaDough {

    private int PizzaDough;
    public int ktoraPizza; //dodałem zmienną pomocniczą public


    @Override //przygotowanie pizzy
    public void preparePizza() {
        preparePizzaDough();
        getIngredients();       //pobieram składniki
        System.out.println("Wkładam pizze do pieca ");
        System.out.println("Wyciągam pizze z pieca i kroję\n****MARGHERITA GOTOWA****");
    }

    @Override //pobieranie składników
    public List<String> getIngredients() {
        List<String> skladnikiKonkretnejPizzy = new ArrayList<>();
        skladnikiKonkretnejPizzy.add("Ser");
        skladnikiKonkretnejPizzy.add("Bazylia");
        System.out.println("Dodaję " + skladnikiKonkretnejPizzy);
        return skladnikiKonkretnejPizzy;
    }

    public Margherita(int pizzaDough) {
        ktoraPizza = pizzaDough;        //w tym konstruktorze pobieram parametr który określa na jakim cieście upiec pizze
    }


    @Override
    public void preparePizzaDough() {           //a tu w zalerzności od pobranego parametru przygotowuje odpowiednie ciasto
        switch (ktoraPizza) {
            case 1:
                AllAmericanThin allAmericanThin = new AllAmericanThin();
                allAmericanThin.preparePizzaDough();
                break;
            case 2:
                ChessyBites chessyBites = new ChessyBites();
                chessyBites.preparePizzaDough();
                break;
            case 3:
                FeelGoodFlatbread feelGoodFlatbread = new FeelGoodFlatbread();
                feelGoodFlatbread.preparePizzaDough();
                break;
            case 4:
                GlutenFree glutenFree = new GlutenFree();
                glutenFree.preparePizzaDough();
                break;
            case 5:
                Pan pan = new Pan();
                pan.preparePizzaDough();
                break;
            case 6:
                StuffedCrust stuffedCrust = new StuffedCrust();
                stuffedCrust.preparePizzaDough();
                break;

        }
    }
}
