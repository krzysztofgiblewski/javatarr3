package com.sda.oop.zadanie05;

public class StuffedCrust implements PizzaDough {
    @Override
    public void preparePizzaDough() {
        System.out.println("Przygotowywanie ciasta StuffedCrust");
    }
}
