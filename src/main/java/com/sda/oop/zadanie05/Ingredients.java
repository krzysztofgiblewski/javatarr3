package com.sda.oop.zadanie05;

import java.util.List;

/**
 * Przygotuj interfejs o nazwie Ingredients do pobierania składników pizzy
 * posiadający metodę List<String> getIngredients() zwracający listę składników
 * i zaimplementuj go w każdej pizzy.
 */

public interface Ingredients {
    List<String> getIngredients();

}
