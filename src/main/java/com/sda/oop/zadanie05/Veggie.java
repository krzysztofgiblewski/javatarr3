package com.sda.oop.zadanie05;

import java.util.ArrayList;
import java.util.List;

/**
 * PIZZA VEGA
 */

public class Veggie implements Pizza, PizzaDough, Ingredients {
    private int PizzaDough;
    public int ktoraPizza;


    @Override //przygotowanie pizzy
    public void preparePizza() {
        preparePizzaDough();
        getIngredients();       //pobieram składniki
        System.out.println("Wkładam pizze do pieca ");
        System.out.println("Wyciągam pizze z pieca i kroję\n****VEGGIE GOTOWA****");
    }

    @Override //pobieranie składników
    public List<String> getIngredients() {
        List<String> skladnikiKonkretnejPizzy = new ArrayList<>();
        skladnikiKonkretnejPizzy.add("Ser");
        skladnikiKonkretnejPizzy.add("Bazylia");
        skladnikiKonkretnejPizzy.add("Pieczarki");
        System.out.println("Dodaję "+skladnikiKonkretnejPizzy);
        return skladnikiKonkretnejPizzy;
    }

    public Veggie(int pizzaDough) {
        ktoraPizza = pizzaDough;
    }


    @Override //przygotowanie ciasta
    public void preparePizzaDough() {
        switch (ktoraPizza) {
            case 1:
                AllAmericanThin allAmericanThin = new AllAmericanThin();
                allAmericanThin.preparePizzaDough();
                break;
            case 2:
                ChessyBites chessyBites = new ChessyBites();
                chessyBites.preparePizzaDough();
                break;
            case 3:
                FeelGoodFlatbread feelGoodFlatbread = new FeelGoodFlatbread();
                feelGoodFlatbread.preparePizzaDough();
                break;
            case 4:
                GlutenFree glutenFree = new GlutenFree();
                glutenFree.preparePizzaDough();
                break;
            case 5:
                Pan pan = new Pan();
                pan.preparePizzaDough();
                break;
            case 6:
                StuffedCrust stuffedCrust = new StuffedCrust();
                stuffedCrust.preparePizzaDough();
                break;

        }


    }



}
