package com.sda.oop.zadanie05;

/**
 * interfejs Przygotowanie Pizzy
 */
public interface Pizza extends PizzaDough {
    void preparePizza();

}
