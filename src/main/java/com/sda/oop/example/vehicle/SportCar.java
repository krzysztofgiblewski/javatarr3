package com.sda.oop.example.vehicle;

import com.sda.oop.example.engines.Engine;

//extends rozszerza klase Car
public class SportCar extends Car{
    public SportCar(String colour, Engine engine) { //dodane przy pomocy Generatora sam to dodał bo jest extends Car
        super(colour, engine);
        numberOfSeats=2;
    }
    public void addNitroToEngine(){
        System.out.println("Strzał z nitro");
    }
@Override
    public void startEngine(){
        System.out.println("sprawdzanie ciśnienia w silniku");
        super.startEngine(); //powinna się odwołać do klasy bazowej

    }
}
