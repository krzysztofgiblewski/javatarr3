package com.sda.oop.example.vehicle;

import com.sda.oop.example.engines.Engine;

public class SuperSportCar extends SportCar{
    public SuperSportCar(String colour, Engine engine) {
        super(colour,engine);
    }
}
