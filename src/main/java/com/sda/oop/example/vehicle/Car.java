package com.sda.oop.example.vehicle;

import com.sda.oop.example.engines.Engine;
import lombok.Getter;
import lombok.ToString;

/**
 * klasa bazowa dla wszystkich samochodów
 */
@ToString
public class Car {
    @Getter
    private boolean engineRunning;
    @Getter
    private String colour;
@Getter
private Engine engine;
@Getter
protected int numberOfSeats=5; //protected żeby sportCar mugł określiś ilość miejsc

    public Car(String colour, Engine engine) {
        this.colour = colour;
        this.engine = engine; //jak ktos przekarze silnik to this wskarze ze to ta zmienna
    }

    public void startEngine(){
        /**
         * start engine
         */
        System.out.println("uruchomiamy silnik samochodu");
        engine.startEngine();
        engineRunning=true;

    }

    /**
     * stop engine
     */
    public void sotpEngine(){
        System.out.println("zatrzymywanie silnika samochodu");
        engineRunning=false;
    }

}
