package com.sda.oop.example.engines;

/**
 * uruchamianie silnika elektrycznego
 */
public class ElectricEngine extends Engine implements EnginInterface {
    @Override
    public void startEngine() {
        System.out.println("Uruchamianie silnika elektrycznego");

    }

    @Override
    public int getOilLevel() {
        return 0;
    }

    @Override
    public int getCoolingFluidLevel() {
        return 0;
    }
}
