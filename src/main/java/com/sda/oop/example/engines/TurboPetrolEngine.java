package com.sda.oop.example.engines;

public class TurboPetrolEngine extends Engine {
    @Override
    public void startEngine() {

    }

    @Override
    public int getOilLevel() {
        return 55;
    }

    @Override
    public int getCoolingFluidLevel() {
        return 55;
    }

}
