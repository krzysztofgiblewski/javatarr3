package com.sda.oop.example.engines;

public abstract class Engine implements EnginInterface {

    private float capacility;
    public abstract void startEngine();

    public float getCopacity(){
        return capacility;
  }


}
