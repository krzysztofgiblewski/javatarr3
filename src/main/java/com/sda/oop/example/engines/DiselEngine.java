package com.sda.oop.example.engines;

/**
 * uruchamianie silnika disla
 */

public class DiselEngine extends Engine {
    @Override
    public void startEngine() {
        System.out.println("Uruchamianie silnika disla");


    }

    @Override
    public int getOilLevel() {
        return 100;
    }

    @Override
    public int getCoolingFluidLevel() {
        return 100;
    }
}
