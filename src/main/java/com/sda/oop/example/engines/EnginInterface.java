package com.sda.oop.example.engines;

public interface EnginInterface {
    int getOilLevel();

    int getCoolingFluidLevel();

}
