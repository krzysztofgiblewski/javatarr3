package com.sda.oop.example;

import com.sda.oop.example.engines.*;
import com.sda.oop.example.vehicle.Car;
import com.sda.oop.example.vehicle.SportCar;
import com.sda.oop.example.vehicle.SuperSportCar;

import java.util.ArrayList;
import java.util.List;

public class Main {
    public static void main(String[] args) {
        Engine petrolEngine = new PetrolEngine();
        Car samochod = new Car("czerwony", new PetrolEngine());
        //uruchamiamy silnik
        samochod.startEngine();
        System.out.println(samochod.isEngineRunning());
        //gasimy silnik
        samochod.sotpEngine();
        System.out.println(samochod.isEngineRunning());
        System.out.println(samochod.toString());

        Engine electricEngin = new ElectricEngine();
        SportCar samochodSportowy = new SportCar("czerwony", electricEngin);
        Car samochodSportowy1 = new SportCar("czerwony", petrolEngine); //nowy Car ale z klasy SportCar
//rzutowanie jak z zmiennymi bo to zrobione z Car a nie SportCar
//        ((SportCar) samochodSportowy1).addNitroToEngine();

        samochod.startEngine();
        samochodSportowy.startEngine();
        samochodSportowy1.startEngine();

        System.out.println("******************************demonstracja dziedziczenia*****************************");

        Car samochod1 = new Car("czarny", new PetrolEngine());
        SportCar samochod2 = new SportCar("czerwony", new DiselEngine());
        SuperSportCar samochod3 = new SuperSportCar("biały", new ElectricEngine());
        SuperSportCar samochod4 =new SuperSportCar("zielony",new TurboPetrolEngine());

        List<Car> samochody = new ArrayList<>();
        samochody.add(samochod1);
        samochody.add(samochod2);
        samochody.add(samochod3);
        samochody.add(samochod4);
        for (Car car : samochody) {
            car.startEngine();
        }

    }
}
