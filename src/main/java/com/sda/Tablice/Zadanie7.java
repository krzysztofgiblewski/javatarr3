package com.sda.Tablice;

public class Zadanie7 {

    public static void main(String[] args) {
        int[] tablica = new int[]{20, 30, 50, 60, 9};
        String tablicaWPostaciTekstowej = new Zadanie7().printArray(tablica);
        System.out.print(tablicaWPostaciTekstowej);

    }

    public String printArray(int[] array) {
        String rezultat = "{ ";
        for (int i = 0; i < array.length; i++) {
            rezultat += array[i];
            if (i != array.length - 1) {
                rezultat += ", ";
            }
        }
        rezultat += " }";
        return rezultat;


    }
}
