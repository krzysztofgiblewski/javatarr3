package com.sda.Tablice;
/*Napisz program, który sprawdza czy podane 2 tablice są identyczne. Sygnatura
metody sprawdzającej jest następująca
public boolean equals(int[] array1, int[] array2)*/

import java.util.Arrays;
import java.util.Scanner;

public class Zadanie9 {

    public static void main(String[] arg){
      int[] tablicaPierwsza=new int[5];
      int[] tablicaDruga=new  int[5];
        System.out.println("podaj pieć elementów tablicy");
        Scanner scanner=new Scanner(System.in);
      for (int i=0; i<5;i++){
          System.out.print(" podaj liczbe: ");
          tablicaPierwsza[i]=scanner.nextInt();
      }

        System.out.println("podaj kolejne pieć elementów tablicy");
        for (int i=0; i<5;i++){
            System.out.print(" podaj liczbe: ");
            tablicaDruga[i]=scanner.nextInt();
        }
        System.out.println("tablica pierwsza");

        for (int i=0; i<5;i++){
            System.out.print(tablicaPierwsza[i]+" ");
        }
        System.out.println("");
        System.out.println("tablica druga");
        for (int i=0; i<5;i++){
            System.out.print(tablicaDruga[i]+" ");
        }
        boolean czyTakieSame= Arrays.equals(tablicaPierwsza,tablicaDruga);
        if (czyTakieSame==true){
            System.out.println("\ntablice są takie same");
        }else {
            System.out.println("Tablice się rużnią");
        }


    }

}
