package com.sda.Tablice;

import java.util.Arrays;

public class Zadanie2 {
    public static void main(String[] args) {
        float[] tablica =new float[5];
        tablica[0] =4.5f;// f żutowanie na float
        tablica[1] =(float)5.5; //to też żutowanie
        tablica[2] =6;
        tablica[3] =8.5f;
        tablica[4] =77.7f;

        String tablicaWPostaciTekstowej = Arrays.toString(tablica);
        System.out.print(tablicaWPostaciTekstowej);
    }
}
