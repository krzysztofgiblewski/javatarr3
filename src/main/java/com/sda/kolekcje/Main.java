package com.sda.kolekcje;

import java.util.*;

public class Main {
    public static void main(String[] args) {
        //tworzenie listy
        List<String> listString = new ArrayList<String>();
        //dodaje elementy do listy
        listString.add("Krzysztof");
        listString.add("java");
        //pobieranie elementu z listy
        listString.get(0);
        listString.get(1);
        //dodawanie na konkretnej pozycji
        listString.add(0, "nowyElement");
        //sprawdzanie długości
        int iloscElementowWLiscie = listString.size();
        //wyswietlanie wszystkich elementow
        for (int i = 0; i < listString.size(); i++) {
            System.out.println(listString.get(i));
            //lub strumieniem ale od java 8
//             listString.forEach(element->System.out.println(element));
        }
//pobranie ostatniego elementu
        listString.get(listString.size() - 1);
//uzuwanie HashSet
        HashSet<String> worekMikolaja = new HashSet<String>();
        boolean czyDodano= worekMikolaja.add("java"); //czyDodano ma wartosc true
        czyDodano = worekMikolaja.add("java"); //czyDodano ma wartosc false bo w hashSet nie mogą sie powtażać elementy
// usunięcie wszystkich elementow
        worekMikolaja.clear();
        //rozmiar
        worekMikolaja.size();
        // usuwanie konkretny obiekt
        worekMikolaja.remove("java");
//petla for icz bo nie mają indeksu wiec po elementach
        for (String elementKolekcji : worekMikolaja) {
            System.out.println(elementKolekcji);
        }
// HasHMap
        HashMap<Integer,String> mapaTekstowa =new HashMap<Integer, String>();
        mapaTekstowa.put(0,"zero"); //dodaje element do mapy najpierw klucz potem przypisana wartosc
        mapaTekstowa.put(1,"jeden"); //dodaje element do mapy najpierw klucz potem przypisana wartosc
        mapaTekstowa.put(3,"trzy"); //dodaje element do mapy najpierw klucz potem przypisana wartosc
        mapaTekstowa.put(8,"osiem"); //dodaje element do mapy najpierw klucz potem przypisana wartosc

        String wartosc =mapaTekstowa.get(0);
        System.out.println(wartosc);
        int rozmiar =mapaTekstowa.size(); //pobranie rozmiaru mapy
        //wyswietlanie wszystkich kluczy w mapie
        for (Integer klucz:  mapaTekstowa.keySet()) {
            System.out.print(klucz+" ");
        }
    //do sprawdzania par klucz-element
        for (Map.Entry<Integer,String> elementMapy:mapaTekstowa.entrySet()) {
            System.out.println("klucz "+elementMapy.getKey()+" wartosc "+ elementMapy.getValue());
        }
        //sprawdza czy klucz istnieje w mapie (nie moga sie powtarzac)
        boolean czyIstniejeKlucz =mapaTekstowa.containsKey(3);
        // czy mapa zaiwera element(moga sie powtarzac)
        boolean czyIstniejeElement =mapaTekstowa.containsValue("java");

        //korzystanie z kolejki
       PriorityQueue<String> kolejka =new PriorityQueue<String>();
       //dodanie
        kolejka.add("java");
        kolejka.add("program");
        kolejka.add("dane");
        kolejka.add("tekst");
        //pobranie elementu ale bez usuniecia z tej kolejki
        String elementZKolejki1 = kolejka.peek();
        //pobieram element i usuwam go z kolejki wiec kolejka ma o ten element mniej
        String elementZKolejki2 = kolejka.poll();
        int iloscElementowWKolejce = kolejka.size();

    }
}
