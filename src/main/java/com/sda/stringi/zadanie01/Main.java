package com.sda.stringi.zadanie01;

import java.util.Scanner;

/*Przygotuj program, który pobiera od użytkownika z konsoli tekst i wyświetla jego
długość.*/
public class Main {
    public static void main(String[] args) {
        String pobranyTekstZKonsoli;
        Scanner scanner=new Scanner(System.in);
        System.out.println("Wpisz jakiś tekst");
        pobranyTekstZKonsoli=scanner.nextLine();
        int dlugoscCiaguLiczbowego;
        dlugoscCiaguLiczbowego=pobranyTekstZKonsoli.length();
        System.out.println("Podany przez ciebie ciąg znaków ma ich dokładnie: "+dlugoscCiaguLiczbowego);
    }
}
