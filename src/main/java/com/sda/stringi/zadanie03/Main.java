package com.sda.stringi.zadanie03;

import java.util.Scanner;

/*Przygotuj program, który pobiera od użytkownika hasło i je wyświetla w postaci
niejawnej zgodnie z następującymi zasadami
a. Pierwszy znak zostaje wyświetlona
b. Wszystkie znak “w środku” zostają podczas wyświetlania zamienione na “*”
c. Ostatni znak zostaje wyświetlony
Przykładowo użytkownik wprowadza tekst: Zażółć gęślą jaźń
Program wyświetla tekst: Z***************ń*/
public class Main {
    public static void main(String[] args) {


    String slowoOdUrzytkownika;
        System.out.println("wpisz jakieś słowo które przepisze tak żeby była pierwsza litera gwiazdki i ostatnia litera");
    Scanner scanner = new Scanner(System.in);
    slowoOdUrzytkownika = scanner.nextLine();
    int iloscLiterWSlowieUrzytkownika;
    iloscLiterWSlowieUrzytkownika = slowoOdUrzytkownika.length();
    char pierwszaLitera;
    char ostatniaLitera;

    String gwiazdka="*";

    String srodekHasla = "";

    pierwszaLitera= (char) slowoOdUrzytkownika.codePointAt(0);
    for (int i=2;i<iloscLiterWSlowieUrzytkownika;i++){
        srodekHasla=gwiazdka+srodekHasla;
    }
    ostatniaLitera= (char) slowoOdUrzytkownika.codePointAt(iloscLiterWSlowieUrzytkownika-1);
        System.out.println(pierwszaLitera +srodekHasla+ostatniaLitera);
}
}