package com.sda.stringi.zadanie02;

import java.lang.reflect.Array;
import java.util.Scanner;

/*Przygotuj program, który pobiera od użytkownika słowo i wyświetla pierwszą i
ostatnią literę wprowadzonego słowa.*/
public class Main {
    public static void main(String[] args) {
        String slowoOdUrzytkownika;
        System.out.println("wpisz jakieś słowo");
        Scanner scanner = new Scanner(System.in);
        slowoOdUrzytkownika = scanner.nextLine();
        int iloscLiterWSlowieUrzytkownika;
        iloscLiterWSlowieUrzytkownika = slowoOdUrzytkownika.length();
        char pierwszaLitera;
        char ostatniaLitera;
       pierwszaLitera= (char) slowoOdUrzytkownika.codePointAt(0);
       ostatniaLitera= (char) slowoOdUrzytkownika.codePointAt(iloscLiterWSlowieUrzytkownika-1);
        System.out.println("pierwsza litera twojego słowa: "+pierwszaLitera);
        System.out.println("ostatnia litera podanego słowa: "+ostatniaLitera);
    }

}

