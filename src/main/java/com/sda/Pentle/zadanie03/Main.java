package com.sda.Pentle.zadanie03;
//Zadeklaruj 10 elementową tablicę
//        int[] array = new int[] {4, 6, 2, 3, 7, 2, 11, 99, 1, 10001}
//        i wykorzystując pętlę for wyświetl poszczególne elementy tablicy w postaci
//        array[0] = 4
//        array[1] = 6
//        array[2] = 2
//        array[3] = 3
//        array[4] = 7
//        array[5] = 2
//        array[6] = 11
//        array[7] = 99
//        array[8] = 1
//        array[9] = 10001
public class Main {
    public static void main(String[] args) {
        int[] array = new int[] {4, 6, 2, 3, 7, 2, 11, 99, 1, 10001};
        for (int i=0;i<array.length;i++){
            String msg="array[%s] = %s";                                //specjalna metoda formatowania stringów
            System.out.println( String.format(msg, i,array[i]));        //
//            System.out.println("array["+array[i]+"] = "+array[i]);
        }
    }
}
