package com.sda.Pentle.zadanie04;
/*
 Napisz program który losuje liczby naturalne i wyświetla wylosowane wartości
         a. losuje 7 liczb naturalnych z zakresu <1-49>. Wykorzystaj klasę Random do
        generowania liczb pseudolosowych.
        Wskazówka:
        Random random = new Random();
        random.nextInt(100); // losuje liczbę z zakresu <0,99>
        b. zmodyfikuj program tak, aby losował tyle liczb naturalnych ile wskaże
        użytkownik, a następnie je wyświetlał
        c. dodaj do programu tablicę i zapisz wszystkie wylosowane wartości w tablicy.
        Następnie korzystając z pętli for wyświetl wylosowane wartości znajdujące się
        w tablicy.
        d. Zmodyfikuj program tak, aby użytkownik podawał zakres losowania liczb (min,
        max)
*/

import com.sda.Utils;

import java.util.HashSet;
import java.util.Random;

public class Main {


    public static void main(String[] args) {
        int ileLiczWylosowac;
        int odIluLosowac;
        int doIluLosowac;
        System.out.print("Ile liczb wylosować? ");
        ileLiczWylosowac = Utils.pobierzLiczbeZKonsoli();
        System.out.print("od jakiej liczby losować? ");
        odIluLosowac = Utils.pobierzLiczbeZKonsoli();
        System.out.print("do jakiej liczby losować? ");
        doIluLosowac = Utils.pobierzLiczbeZKonsoli();
        int ruznica = doIluLosowac - odIluLosowac;
        int[] tablicaWylosowanychNumerkow = new int[ileLiczWylosowac];
        Random wylosowanaLiczba = new Random();
//HashSet bo nie mogą się powtarzać elemęty kolekcji
        HashSet<Integer> listaNumerkow = new HashSet<>();
        //zeby nie losował wiecej niż ma liczb
        if (ruznica >= ileLiczWylosowac) {
            do {
                int wylosowanyNumerek = wylosowanaLiczba.nextInt(ruznica + 1);
                listaNumerkow.add(wylosowanyNumerek += odIluLosowac);
            } while (listaNumerkow.size() < ileLiczWylosowac);
            //poniewarz liczby moga sie losować takie same to losuje doputy nie znajdzie wystarczajaco durzo liczb
            System.out.println("wylosowana Liczba " + listaNumerkow.toString());

            for (int i = 0; i < ileLiczWylosowac; i++) {

                    if (listaNumerkow.equals(i)==true){
                        tablicaWylosowanychNumerkow[i] = i;

            }

            }
            for (int i = 0; i < tablicaWylosowanychNumerkow.length; i++) {
                System.out.print(tablicaWylosowanychNumerkow[i] + " ");

            }

        } else if (ruznica < ileLiczWylosowac) {
            System.out.println("za durzo licz chcesz wylosować");
        }

    }


}
