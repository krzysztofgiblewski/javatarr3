package com.sda.Pentle.zadanie02;

import com.sda.Utils;

import java.util.Scanner;

/*
Napisz program, który wyświetla n liczb naturalnych, gdzie ilość liczb wprowadza
użytkownik za pomocą konsoli.
*/
public class Main {
    public static void main(String[] args) {
        int liczban = Utils.pobierzLiczbeZKonsoli();
        for (int i = 1; i <= liczban; i++) {
            System.out.println(i);

        }
    }
}
