package com.sda.calc;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner skaner = new Scanner(System.in);
        System.out.println("program wykonuje operację matematyczną dla dwóch podanych licz");
        System.out.print("podaj pierwsza cyfrę: ");
        int liczba1 = skaner.nextInt();
        System.out.print("podaj 2 liczbę: ");
        int liczba2 = skaner.nextInt();
        System.out.print("podaj operację (+-*/");
        skaner.nextLine();                              //to po to żęby bufor skanera się nie  przepełniał zczytujemy pusy znak
        String operacja = skaner.nextLine();
        Float wynikOperacji = null;
        if (liczba1 > 0 && liczba2 > 0) {
            switch (operacja.charAt(0)) {
                case '*':
                    wynikOperacji = (float) liczba1 * liczba2;
                    break;
                case '+':
                    wynikOperacji = (float) liczba1 + liczba2;
                    break;
                case '-':
                    wynikOperacji = (float) liczba1 - liczba2;
                    break;
                case '/':
                    if (liczba2 == 0) {
                        System.out.println("Nie dziel przez ZERO");
                    } else {
                        wynikOperacji = (float) liczba1 / liczba2;
                    }
                    break;
                default:
                    System.out.println("nie wiem o co ci chodzi nie znam takiego działania");
            }
            if (wynikOperacji != null) {
                System.out.println("Wynik operacji " + wynikOperacji);
            } else {
                System.out.println("podałeś liczbe ujemną nie umiem tego zrobić");
            }
        }
    }
}
