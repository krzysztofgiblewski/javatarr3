package com.sda.Warunki.Zadanie04;

import java.util.Scanner;

/**
 * Przygotuj aplikację, która pobierze od użytkownika 2 liczby zmiennoprzecinkowe
 * (float lub double) i sprawdzi czy wprowadzone liczby są takie same z dokładnością
 * do 3 miejsca po przecinku.
 * Przykładowe dane wejściowe: 11,345119 oraz 11,34579
 * Wynik działania programu:
 * “Wprowadzone liczby są takie same”
 * bo
 * 11,345119 = 11,34579
 */
public class Main {
    public static void main(String[] args) {
float pierwszaLiczba;
double drugaLiczba;
        System.out.println("podaj pierwszą liczbę");
        Scanner pobieramLiczbyOdUrzytkownika=new Scanner(System.in);
        pierwszaLiczba=pobieramLiczbyOdUrzytkownika.nextFloat();
        System.out.println("podaj drugą liczbę");
        drugaLiczba=pobieramLiczbyOdUrzytkownika.nextDouble();
        if (pierwszaLiczba==(float)drugaLiczba)
            System.out.println("Wprowadzone liczby są takie same bo "+pierwszaLiczba+" = "+drugaLiczba);
        else System.out.println("podane liczby są rużne bo "+pierwszaLiczba+" <> "+drugaLiczba);
    }
}
