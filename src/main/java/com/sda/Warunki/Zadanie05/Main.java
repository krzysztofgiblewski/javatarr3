package com.sda.Warunki.Zadanie05;

import java.util.Scanner;

/**
 * Przygotuj aplikację, która pobiera od użytkownika dowolny tekst i sprawdza czy jest
 * długości co najmniej 10 znaków. Jeśli wprowadzony tekst jest krótszy niż 10 znaków
 * program wyświetla komunikat:
 * “Wprowadzony tekst jest za krótki”
 * Jeśli natomiast tekst jest długości 10 znaków lub więcej, program wyświetla
 * komunikat:
 * “Wprowadzony tekst “<tekst_podany_przez_użytkownika>” spełnia
 * kryteria”
 * Przykładowe dane wejściowe: kot
 * Wynik działania programu:
 * “Wprowadzony tekst jest za krótki”
 * Przykładowe dane wejściowe: kot europejski
 * Wynik działania programu:
 * “Wprowadzony tekst “kot europejski” spełnia kryteria”
 */
public class Main {
    public static void main(String[] args) {
        String tekst;
        System.out.println("wprowadź minimum 10 znakowy tekst");
        Scanner pobierz=new Scanner(System.in);
        tekst=pobierz.next();
        if (tekst.length()<10)
            System.out.println("Wprowadzony tekst jest za krótki ");
        else System.out.println(tekst+" spełnia kryteria");
    }
}
