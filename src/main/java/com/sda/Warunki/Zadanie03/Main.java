package com.sda.Warunki.Zadanie03;

import com.sda.Utils;

//        Przygotuj program, który pobierze od użytkownika 3 liczby i sprawdzi, która liczba
//        jest największa, a która najmniejsza. Program powinien na zakończenie działania
//        wyświetlić informację o liczbie maksymalnej i minimalnej
//        Przykładowe dane wejściowe: 4, 99, 11
//        Wynik działania programu:
//        “Największa liczba to 99, a najmniejsza to 4”
public class Main {
    public static void main(String[] args) {
        int liczba1 = Utils.pobierzLiczbeZKonsoli();
        int liczba2 = Utils.pobierzLiczbeZKonsoli();
        int liczba3 = Utils.pobierzLiczbeZKonsoli();
        int najWieksza;
        int najmniejsza;

        if (liczba1 < liczba2) {
            if (liczba1 < liczba3) {
                najmniejsza = liczba1;
            } else {
                najmniejsza = liczba1;
            }
        } else {
            if (liczba2 < liczba3) {
                najmniejsza = liczba2;
            } else {
                najmniejsza = liczba3;
            }
        }

        if (liczba1 > liczba2) {
            if (liczba1 > liczba3) {
                najWieksza = liczba1;
            } else {
                najWieksza = liczba1;
            }
        } else {
            if (liczba2 > liczba3) {
                najWieksza = liczba2;
            } else {
                najWieksza = liczba3;
            }

        }
        System.out.println("najminiejsza liczba to " + najmniejsza);
        System.out.println("najwieksza liczba to: " + najWieksza);
    }


}
