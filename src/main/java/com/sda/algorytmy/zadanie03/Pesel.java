package com.sda.algorytmy.zadanie03;

public class Pesel {

    int pesel,rokUrodzenia,miesiacUrodzenia,dzienUrodzenia,liczbaPorzadkowa,sumaKontrolna;
    String plec;


    public Pesel(int rokUrodzenia, int miesiacUrodzenia, int dzienUrodzenia, int liczbaPorzadkowa, int sumaKontrolna, String plec) {
        this.rokUrodzenia = rokUrodzenia;
        this.miesiacUrodzenia = miesiacUrodzenia;
        this.dzienUrodzenia = dzienUrodzenia;
        this.liczbaPorzadkowa = liczbaPorzadkowa;
        this.sumaKontrolna = sumaKontrolna;
        this.plec = plec;
    }



    public int getRokUrodzenia() {
        return rokUrodzenia;
    }

    public void setRokUrodzenia(int rokUrodzenia) {
        this.rokUrodzenia = rokUrodzenia;
    }

    public int getMiesiacUrodzenia() {
        return miesiacUrodzenia;
    }

    public void setMiesiacUrodzenia(int miesiacUrodzenia) {
        this.miesiacUrodzenia = miesiacUrodzenia;
    }

    public int getDzienUrodzenia() {
        return dzienUrodzenia;
    }

    public void setDzienUrodzenia(int dzienUrodzenia) {
        this.dzienUrodzenia = dzienUrodzenia;
    }

    public int getLiczbaPorzadkowa() {
        return liczbaPorzadkowa;
    }

    public void setLiczbaPorzadkowa(int liczbaPorzadkowa) {
        this.liczbaPorzadkowa = liczbaPorzadkowa;
    }

    public int getSumaKontrolna() {
        return sumaKontrolna;
    }

    public void setSumaKontrolna(int sumaKontrolna) {
        this.sumaKontrolna = sumaKontrolna;
    }

    public String getPlec() {
        return plec;
    }

    public void setPlec(String plec) {
        this.plec = plec;
    }
}
