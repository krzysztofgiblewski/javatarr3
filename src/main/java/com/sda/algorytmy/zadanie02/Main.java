package com.sda.algorytmy.zadanie02;

import com.sda.Utils;

import java.util.Scanner;

/*Przygotuj program który pobiera od użytkownika trzy liczby rzeczywiste oznaczające
długości odcinków. Jeśli z podanych odcinków można zbudować trójkąt, to algorytm
określa, czy dany trójkąt jest prostokątny. W przeciwnym razie pojawia się
komentarz, że podane liczby nie mogą być długościami boków trójkąta*/


public class Main {
    public static void main(String [] arg){
        Scanner scanner=new Scanner (System.in);
        System.out.print("podaj długość pierwszego boku: ");
        double bokPierwszy =scanner.nextDouble();
        System.out.print("podaj długość drugiego boku: ");
        double bokDrugi = scanner.nextDouble();
        System.out.print("podaj długość trzeciego z boków: ");
        double bokTrzeci= scanner.nextDouble();
         double przeciwProstokatna;
        double przyprostokatna1;
        double przyprostokatna2;

        if (bokPierwszy>bokDrugi){
            przeciwProstokatna=bokPierwszy;
            przyprostokatna1=bokDrugi;
        }else {
            przeciwProstokatna=bokDrugi;
            przyprostokatna1=bokPierwszy;
        }
        if ((bokTrzeci > przeciwProstokatna)) {
            przeciwProstokatna=bokTrzeci;
            przyprostokatna2=bokDrugi ;//bo wczesniej to pierwszy był najdluzszy
        }else {
            przyprostokatna2=bokTrzeci;
        }

        System.out.println("najdłuższy bok "+przeciwProstokatna);
        System.out.println("krutszy bok "+przyprostokatna1);
        System.out.println("i drugi krutszy bok "+przyprostokatna2);

        double trujkat= (przyprostokatna1*przyprostokatna1)+(przyprostokatna2*przyprostokatna2); //Math.pow(co_do_potegi, do_ktorej_potegi)
        double trujkatSprawdze=Math.round(przeciwProstokatna*przeciwProstokatna);

        if (trujkatSprawdze==trujkat){
            System.out.println("z podanych długości można narysować trujkąt prostokątny");
        }else {
            System.out.println(" a kwadrat + b kwadrat "+trujkat+" powinno być równe c kwadrat a wynosi "+trujkatSprawdze);
            System.out.println(" najdłuższy bok powinien mieć "+Math.sqrt(trujkat)+" długości ");

        }



    }

}
