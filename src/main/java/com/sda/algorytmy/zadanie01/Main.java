package com.sda.algorytmy.zadanie01;

import com.sda.Utils; //import do naszego utils

/*
* Przygotuj aplikację, która umożliwia wprowadzenie 10 liczb przez użytkownika.
Spośród wprowadzonych liczb program wyszukuje najmniejszą i największą liczbę,
oblicza średnią arytmetyczną, średnią geometryczną oraz wyświetla obliczone
wartości użytkownikowi.
*/
public class Main {

    public static void main(String[] arg){
        System.out.println("wprowadź 10 licz ");
       int[] pobierzLiczbe=new int[10];
       for (int i=0;i<10;i++){
           int liczbaPobrana=Utils.pobierzLiczbeZKonsoli();     // wykorzystuje importowana klase utils
           pobierzLiczbe[i]=liczbaPobrana;
       }
       //w petli wyswietlam zawartość tablicy
       for (int i=0;i< pobierzLiczbe.length;i++){
           System.out.print(pobierzLiczbe[i]+" ");
       }


        //średnia arytmetyczną bo nie wiem jak zrobić pierwiastek
        double wartoscSredniejArytmetycznej=0;
        for (int i=0;i<10;i++){
         wartoscSredniejArytmetycznej+= pobierzLiczbe[i];
        }
        wartoscSredniejArytmetycznej=wartoscSredniejArytmetycznej/10;
        System.out.println("\nśrednia arytmetyczna wynosi: "+wartoscSredniejArytmetycznej);


        // najwieksza i najmniejsza
        int najmniejsza=0;
        int najwieksza=0;

        for (int i=0;i<pobierzLiczbe.length;i++){
            najwieksza=    Math.max(pobierzLiczbe[i],pobierzLiczbe[i++]);
        }
        for (int i=0;i<9;i++){
            najmniejsza=    Math.min(pobierzLiczbe[i],pobierzLiczbe[i++]);
            pobierzLiczbe[i++]=najmniejsza;
        }
        System.out.println("\nnajmniejsza liczba: "+najmniejsza);
        System.out.println("największa liczba: "+najwieksza);
    }

}
