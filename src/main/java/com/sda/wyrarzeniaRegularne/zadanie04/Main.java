package com.sda.wyrarzeniaRegularne.zadanie04;
//Napisz aplikację, która sprawdza, czy wprowadzona przez użytkownika
// liczba jest 3  cyfrowa

import com.sda.Utils;

import java.util.regex.Pattern;

public class Main {
    public static void main(String[] args) {
        int liczba = Utils.pobierzLiczbeZKonsoli();
       boolean jestOk= Pattern.matches("^[0-9]{3}$",Integer.toString(liczba));
       if (jestOk){
           System.out.println("liczba jest poprawna");
       }else {
           System.out.println("liczba NIE jest poprawna");
       }

    }
}
