package com.sda.wyrarzeniaRegularne.zadanie01;
//Napisz aplikację, która sprawdza czy wprowadzony przez użytkownika ciąg znaków
//        zawiera tylko i wyłącznie duże litery.

import com.sda.Utils;

import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Napisz aplikację, która sprawdza czy wprowadzony przez użytkownika ciąg znaków
 * zawiera tylko i wyłącznie duże litery.
 */

public class Main {
    public static void main(String[] args) {
        Scanner skaner = new Scanner(System.in);
        System.out.print("wprowadz tekst ");
        String tekst=skaner.nextLine();
        Pattern wzorzec=Pattern.compile("^[A-Z]+$");
        Matcher porownywacz=wzorzec.matcher(tekst);
        if (porownywacz.matches()){
            System.out.print("wprowadzono tekst zawierajacy tylko DUŻE litery");
        }else {
            System.out.println("wprowadzony tekst jest niepoprawny");
        }
    }
}
