package com.sda.wyrarzeniaRegularne.zadanie03;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Napisz aplikację, która sprawdza czy wprowadzony przez użytkownika ciąg znaków
 * zawiera tylko i wyłącznie cyfry.
 */

public class Main {
    public static void main(String[] args) {
        System.out.println("Podaj ciag znaków sprawdze czy zawiera tylko cyfry");
        Scanner scanner=new Scanner(System.in);
        String tektOdUrzytkownika= scanner.nextLine();
        Pattern wzor=Pattern.compile("^[0-9]+$");
        Matcher porownaj=wzor.matcher(tektOdUrzytkownika);
        if (porownaj.matches()){
            System.out.println("Tak to tylko cyfry");
        }else {
            System.out.println("O tu są też litery");
        }
    }
}
