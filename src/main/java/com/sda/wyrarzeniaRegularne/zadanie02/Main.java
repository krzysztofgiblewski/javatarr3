package com.sda.wyrarzeniaRegularne.zadanie02;

import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Napisz aplikację, która sprawdza czy wprowadzony przez użytkownika ciąg znaków
 * zawiera tylko i wyłącznie małe litery.
 */
public class Main {
    public static void main(String[] args) {
        System.out.println("Podaj ciag znaków małymi literami");
        Scanner scanner=new Scanner(System.in);
       String tektOdUrzytkownika= scanner.nextLine();
        Pattern wzor=Pattern.compile("^[a-z]+$");
        Matcher porownaj=wzor.matcher(tektOdUrzytkownika);
        if (porownaj.matches()){
            System.out.println("Tak to małe litery");
        }else {
            System.out.println("O tu są i duże litery");
        }


    }
}
