package com.sda.wyrarzeniaRegularne.zadanie05;

import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Napisz aplikację, która sprawdza, czy wprowadzona przez użytkownika liczba składa
 * się z 3-5 cyfr
 */
public class Main {

    public static void main(String[] args) {
        System.out.println("podaj jakąś liczbę sprawdze czy mieści się między 3 a 5 cyfr");
        Scanner scanner=new Scanner(System.in);
        String liczbaUrzytkownika=scanner.nextLine();
        Pattern wzorujemy=Pattern.compile("^[0-9]{3,5}$");
        Matcher porownanie=wzorujemy.matcher(liczbaUrzytkownika);
        if (porownanie.matches()){
            System.out.println("ok liczba mieści śię między 3 a 5 znaków");
        }else {
            System.out.println("no nie tak ");
        }

    }
}
